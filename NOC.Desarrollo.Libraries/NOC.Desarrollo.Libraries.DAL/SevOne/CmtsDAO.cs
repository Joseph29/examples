﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

using MySql.Data.MySqlClient;


using NOC.Desarrollo.Libraries.Common;
using NOC.Desarrollo.Libraries.Common.SevOne;
using NOC.Desarrollo.Libraries.Core.Persistance;
using NOC.Desarrollo.Libraries.DTL;
using NOC.Desarrollo.Libraries.DTL.SevOne;

namespace NOC.Desarrollo.Libraries.DAL.SevOne
{
    /// <summary>
    /// Clase que contiene los métodos de Select
    /// para obtener información de los CMTS
    /// </summary>
    public class CmtsDAO
    {
        /// <summary>
        /// Método para obtener únicamente los CMTS registrados en SevOne
        /// </summary>
        /// <param name="server">Servidor a consultar</param>
        /// <returns></returns>
        public List<DeviceDTO> GetCmts(ServersEnum server)
        {
            List<DeviceDTO> devices = new List<DeviceDTO>();

            try
            {
                string connectionString = string.Empty;
                #region ConnectionString
                switch (server)
                {
                    case ServersEnum.SevOne:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOneConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne2:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne2ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne3:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne3ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne4:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne4ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne5:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne5ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne6:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne6ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOneCm1:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOneCmConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOneCm2:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOneCm2ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOneCm3:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOneCm3ConnectionString"].ConnectionString;
                        break;
                }
                #endregion

                //Se obtienen los CMTS del server solicitado 
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    string commandString = @"SELECT DISTINCT
                                                 dev.id
                                                ,dev.name
                                                ,dev.description
                                                ,dev.mgt_address
                                                ,dev.ro_community
                                            FROM net.deviceinfo as dev
											INNER JOIN local.device_object as devo ON dev.id = devo.device_id
                                            WHERE 1 = 1";

                    #region Conditions
                    switch (server)
                    {
                        case ServersEnum.SevOne:
                        case ServersEnum.SevOne2:
                        case ServersEnum.SevOne3:
                        case ServersEnum.SevOne4:
                        case ServersEnum.SevOne5:
                        case ServersEnum.SevOne6:
                            commandString += " AND (dev.description like '%CMTS%' or dev.alt_name like '%cmts%')";
                            break;
                        case ServersEnum.SevOneCm1:
                        case ServersEnum.SevOneCm2:
                        case ServersEnum.SevOneCm3:
                            commandString += " AND (devo.description like '%downstream%' OR dev.alt_name like '%cmts%')";
                            break;
                    }
                    #endregion
                    commandString += " ORDER BY dev.name, dev.description";

                    using (MySqlCommand command = new MySqlCommand(commandString, connection))
                    {
                        devices = DeviceListMapper(command.ExecuteReader());
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                Console.WriteLine(string.Format("GetCmts - {0}", ex.Message));
            }

            return devices;
        }

        /// <summary>
        /// Método para obtener las interfaces por CMTS
        /// </summary>
        /// <param name="server">Servidor a consultar</param>
        /// <param name="deviceId">Inditificador del CMTS</param>
        /// <param name="onlyDownstreams">Indica si únicamente se obtiene los downstrem</param>
        /// <returns></returns>
        public List<DeviceObjectDTO> GetCmtsInterfacesById(ServersEnum server, int deviceId, bool onlyDownstreams)
        {
            List<DeviceObjectDTO> devices = new List<DeviceObjectDTO>();

            try
            {
                string connectionString = string.Empty;
                #region ConnectionString
                switch (server)
                {
                    case ServersEnum.SevOne:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOneConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne2:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne2ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne3:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne3ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne4:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne4ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne5:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne5ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOne6:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOne6ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOneCm1:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOneCmConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOneCm2:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOneCm2ConnectionString"].ConnectionString;
                        break;
                    case ServersEnum.SevOneCm3:
                        connectionString = ConfigurationManager.ConnectionStrings["SevOneCm3ConnectionString"].ConnectionString;
                        break;
                }
                #endregion

                //Se obtienen los CMTS del server solicitado 
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();

                    List<MySqlParameter> parameters = new List<MySqlParameter>();
                    string commandString = @"SELECT 
                                                 devo.id
                                                ,devo.device_id
                                                ,devo.name
                                                ,devo.description
                                                ,devo.is_enabled
                                                ,devo.table_name
                                                ,(SELECT datatable_column FROM net.snmppoll pol WHERE devo.device_id = pol.dev_id AND devo.id = pol.object_id AND indicator_base like 'if%InOctets' ) as ifHCInOctets
                                                ,(SELECT datatable_column FROM net.snmppoll pol WHERE devo.device_id = pol.dev_id AND devo.id = pol.object_id AND indicator_base like 'if%OutOctets' ) as ifHCOutOctets
                                            FROM local.device_object as devo
                                            WHERE 1 = 1
                                            AND devo.device_Id = @deviceId
                                            AND devo.is_enabled = 1";

                    parameters.Add(new MySqlParameter("deviceId", deviceId));

                    if (onlyDownstreams)
                    {
                        #region Conditions
                        switch (server)
                        {
                            case ServersEnum.SevOne:
                            case ServersEnum.SevOne2:
                            case ServersEnum.SevOne3:
                            case ServersEnum.SevOne4:
                            case ServersEnum.SevOne5:
                            case ServersEnum.SevOne6:
                                commandString += " AND (devo.description like '%downstream%' OR devo.description like '%ds%')";
                                break;
                            case ServersEnum.SevOneCm1:
                            case ServersEnum.SevOneCm2:
                            case ServersEnum.SevOneCm3:
                                commandString += " AND (devo.description like '%downstream%' OR devo.description like '%ds%')";
                                break;
                        }
                        #endregion
                    }

                    using (MySqlCommand command = new MySqlCommand(commandString, connection))
                    {
                        command.Parameters.AddRange(parameters.ToArray());
                        devices = DeviceObjectListMapper(command.ExecuteReader());
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                Console.WriteLine(string.Format("GetCmtsInterfacesById - DeviceId {0} - {1}", deviceId, ex.Message));
            }

            return devices;
        }

        /// <summary>
        /// Método para obtener las lecturas de una interfaz
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<DeviceDataDTO> GetCmtsDataByInterfaz(DeviceDataRequestDTO request)
        {
            List<DeviceDataDTO> data = new List<DeviceDataDTO>();
            string commandString = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(request.ColumnIfHcInOctets) && !string.IsNullOrEmpty(request.ColumnIfHcOutOctets))
                {
                    string connectionString = string.Empty;
                    #region ConnectionString
                    switch (request.Server)
                    {
                        case ServersEnum.SevOne:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOneConnectionString"].ConnectionString;
                            break;
                        case ServersEnum.SevOne2:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOne2ConnectionString"].ConnectionString;
                            break;
                        case ServersEnum.SevOne3:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOne3ConnectionString"].ConnectionString;
                            break;
                        case ServersEnum.SevOne4:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOne4ConnectionString"].ConnectionString;
                            break;
                        case ServersEnum.SevOne5:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOne5ConnectionString"].ConnectionString;
                            break;
                        case ServersEnum.SevOne6:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOne6ConnectionString"].ConnectionString;
                            break;
                        case ServersEnum.SevOneCm1:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOneCmConnectionString"].ConnectionString;
                            break;
                        case ServersEnum.SevOneCm2:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOneCm2ConnectionString"].ConnectionString;
                            break;
                        case ServersEnum.SevOneCm3:
                            connectionString = ConfigurationManager.ConnectionStrings["SevOneCm3ConnectionString"].ConnectionString;
                            break;
                    }
                    #endregion

                    //Se obtienen los CMTS del server solicitado 
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    {
                        connection.Open();

                        List<MySqlParameter> parameters = new List<MySqlParameter>();
                        if (string.IsNullOrEmpty(request.ColumnIfHcInOctets) && !string.IsNullOrEmpty(request.ColumnIfHcOutOctets))
                            commandString = string.Format("SELECT CONVERT_TZ(FROM_UNIXTIME(r.time), '+00:00','-05:00'), 0 as IfHcInOctets, r.{1} FROM {2} as r  WHERE 1 = 1 ", request.ColumnIfHcInOctets, request.ColumnIfHcOutOctets, request.TableName);
                        else if (!string.IsNullOrEmpty(request.ColumnIfHcInOctets) && string.IsNullOrEmpty(request.ColumnIfHcOutOctets))
                            commandString = string.Format("SELECT CONVERT_TZ(FROM_UNIXTIME(r.time), '+00:00','-05:00'), r.{0}, 0 asIfHcOutOctets FROM {2} as r  WHERE 1 = 1 ", request.ColumnIfHcInOctets, request.ColumnIfHcOutOctets, request.TableName);
                        else if (!string.IsNullOrEmpty(request.ColumnIfHcInOctets) && !string.IsNullOrEmpty(request.ColumnIfHcOutOctets))
                            commandString = string.Format("SELECT CONVERT_TZ(FROM_UNIXTIME(r.time), '+00:00','-05:00'), r.{0}, r.{1} FROM {2} as r  WHERE 1 = 1 ", request.ColumnIfHcInOctets, request.ColumnIfHcOutOctets, request.TableName);

                        if (request.RangeDateTime != null)
                        {
                            if (request.RangeDateTime.InitialDate.HasValue && request.RangeDateTime.FinalDate.HasValue)
                            {
                                commandString += " AND CONVERT_TZ(FROM_UNIXTIME(r.time), '+00:00','-05:00') BETWEEN @initialDate AND @finalDate";
                                parameters.Add(new MySqlParameter("initialDate", request.RangeDateTime.InitialDate.Value));
                                parameters.Add(new MySqlParameter("finalDate", request.RangeDateTime.FinalDate.Value));
                            }
                            else if (request.RangeDateTime.InitialDate.HasValue)
                            {
                                commandString += " AND CONVERT_TZ(FROM_UNIXTIME(r.time), '+00:00','-05:00') > @initialDate";
                                parameters.Add(new MySqlParameter("initialDate", request.RangeDateTime.InitialDate.Value));
                            }
                            else if (request.RangeDateTime.FinalDate.HasValue)
                            {
                                commandString += " AND CONVERT_TZ(FROM_UNIXTIME(r.time), '+00:00','-05:00') < @finalDate";
                                parameters.Add(new MySqlParameter("finalDate", request.RangeDateTime.FinalDate.Value));
                            }
                        }

                        using (MySqlCommand command = new MySqlCommand(commandString, connection))
                        {
                            command.Parameters.AddRange(parameters.ToArray());
                            data = DeviceDataListMapper(command.ExecuteReader());
                        }

                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                Console.WriteLine(string.Format("GetCmtsDataByInterfaz - TableName {0} - {1}", request.TableName, ex.Message));
            }

            return data;
        }

        #region Mappers
        private List<DeviceDTO> DeviceListMapper(MySqlDataReader reader)
        {
            List<DeviceDTO> devices = new List<DeviceDTO>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        DeviceDTO device = new DeviceDTO();
                        device.DeviceId = ReaderHelper.GetInteger(reader[0]);
                        device.DeviceName = ReaderHelper.GetString(reader[1]);
                        device.DeviceDescription = ReaderHelper.GetString(reader[2]);
                        device.DeviceIpAddress = ReaderHelper.GetString(reader[3]);
                        device.DeviceCommunitySnmp = ReaderHelper.GetString(reader[4]);
                        device.DeviceObjects = new List<DeviceObjectDTO>();
                        devices.Add(device);
                    }
                }
            }

            return devices;
        }

        private List<DeviceObjectDTO> DeviceObjectListMapper(MySqlDataReader reader)
        {
            List<DeviceObjectDTO> devices = new List<DeviceObjectDTO>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        DeviceObjectDTO device = new DeviceObjectDTO();
                        device.DeviceObjectId = ReaderHelper.GetInteger(reader[0]);
                        device.DeviceId = ReaderHelper.GetInteger(reader[1]);
                        device.DeviceObjectName = ReaderHelper.GetString(reader[2]);
                        device.DeviceObjectDescription = ReaderHelper.GetString(reader[3]);
                        device.DeviceObjectIsEnabled = ReaderHelper.GetBoolean(reader[4]);
                        device.DeviceObjectTableName = ReaderHelper.GetString(reader[5]);
                        device.DeviceColumnIfHcInOctets = ReaderHelper.GetString(reader[6]);
                        device.DeviceColumnIfHcOutOctets = ReaderHelper.GetString(reader[7]);
                        device.DeviceObjectData = new List<DeviceDataDTO>();
                        device.DeviceObjectDataMax = new List<DeviceDataDTO>();
                        devices.Add(device);
                    }
                }
            }

            return devices;
        }

        private List<DeviceDataDTO> DeviceDataListMapper(MySqlDataReader reader)
        {
            List<DeviceDataDTO> data = new List<DeviceDataDTO>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        DeviceDataDTO row = new DeviceDataDTO();
                        row.DeviceDataDateTime = ReaderHelper.GetDateTime(reader[0]);
                        row.DeviceDataHcInOctets = ReaderHelper.GetDecimal(reader[1]);
                        row.DeviceDataHcOutOctets = ReaderHelper.GetDecimal(reader[2]);
                        data.Add(row);
                    }
                }
            }

            return data;
        }
        #endregion
    }
}
