﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using NOC.Desarrollo.Libraries.Core.Persistance;
using NOC.Desarrollo.Libraries.DTL.ServAssure;

namespace NOC.Desarrollo.Libraries.DAL.ServAssure
{
    /// <summary>
    /// Clase que contiene los métodos
    /// para guardar el histórico de cada lectura
    /// de las MACs
    /// </summary>
    public class MacHistoryDAO
    {
        private string databaseName = "MacInformation";

        #region Public
        /// <summary>
        /// Método para insertar la información del estado de una 
        /// MAC en una hora
        /// </summary>
        /// <param name="mac"></param>
        /// <returns></returns>
        public int InsertMacHealthByHour(MacHealthDTO mac)
        {
            int id = 0;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                string query = @"INSERT INTO  CM_HOUR_HEALTH (
                                         MAC
                                        ,ACCOUNT_NUMBER
                                        ,HOUR_STAMP
                                        ,TXPOWER_UP
                                        ,RXPOWER_UP
                                        ,RXPOWER_DN
                                        ,CER_DN
                                        ,CCER_DN
                                        ,CER_UP
                                        ,CCER_UP
                                        ,SNR_DN
                                        ,SNR_UP
                                        ,CM_STATUS
                                        ,PCT_TRAFFIC_SDMH_UP
                                        ,PCT_TRAFFIC_DMH_UP
                                        ,PCT_PLANT_SDMH_UP
                                        ,PCT_PLANT_DMH_UP
                                        ,PCT_TRAFFIC_SDMH_DN
                                        ,PCT_TRAFFIC_DMH_DN
                                        ,PCT_PLANT_SDMH_DN
                                        ,PCT_PLANT_DMH_DN
                                        ,PCT_CPU_SDMH
                                        ,PCT_CPU_DMH
                                        ,SUBSCRIBER_FIRST_NAME
                                        ,SUBSCRIBER_LAST_NAME
                                        ,SUBSCRIBER_STREET_NUMBER
                                        ,SUBSCRIBER_STREET_NAME
                                        ,SUBSCRIBER_CITY
                                        ,SUBSCRIBER_PHONE_NUMBER
                                        )
                                     VALUES (
                                         @MAC
                                        ,@ACCOUNT_NUMBER
                                        ,@HOUR_STAMP
                                        ,@TXPOWER_UP
                                        ,@RXPOWER_UP
                                        ,@RXPOWER_DN
                                        ,@CER_DN
                                        ,@CCER_DN
                                        ,@CER_UP
                                        ,@CCER_UP
                                        ,@SNR_DN
                                        ,@SNR_UP
                                        ,@CM_STATUS
                                        ,@PCT_TRAFFIC_SDMH_UP
                                        ,@PCT_TRAFFIC_DMH_UP
                                        ,@PCT_PLANT_SDMH_UP
                                        ,@PCT_PLANT_DMH_UP
                                        ,@PCT_TRAFFIC_SDMH_DN
                                        ,@PCT_TRAFFIC_DMH_DN
                                        ,@PCT_PLANT_SDMH_DN
                                        ,@PCT_PLANT_DMH_DN
                                        ,@PCT_CPU_SDMH
                                        ,@PCT_CPU_DMH
                                        ,@SUBSCRIBER_FIRST_NAME
                                        ,@SUBSCRIBER_LAST_NAME
                                        ,@SUBSCRIBER_STREET_NUMBER
                                        ,@SUBSCRIBER_STREET_NAME
                                        ,@SUBSCRIBER_CITY
                                        ,@SUBSCRIBER_PHONE_NUMBER
                                      )";

                parameters.Add(new SqlParameter("MAC", mac.Mac));
                parameters.Add(new SqlParameter("ACCOUNT_NUMBER", mac.AccountNumber));
                parameters.Add(new SqlParameter("HOUR_STAMP", mac.HourStamp));
                parameters.Add(new SqlParameter("TXPOWER_UP", mac.TxPowerUp));
                parameters.Add(new SqlParameter("RXPOWER_UP", mac.RxPowerUp));
                parameters.Add(new SqlParameter("RXPOWER_DN", mac.RxPowerDn));
                parameters.Add(new SqlParameter("CER_DN", mac.CerDn));
                parameters.Add(new SqlParameter("CCER_DN", mac.CCerDn));
                parameters.Add(new SqlParameter("CER_UP", mac.CerUp));
                parameters.Add(new SqlParameter("CCER_UP", mac.CCerUp));
                parameters.Add(new SqlParameter("SNR_DN", mac.SnrDn));
                parameters.Add(new SqlParameter("SNR_UP", mac.SnrUp));
                parameters.Add(new SqlParameter("CM_STATUS", mac.CmStatus));
                parameters.Add(new SqlParameter("PCT_TRAFFIC_SDMH_UP", mac.PctTrafficSDMHUp));
                parameters.Add(new SqlParameter("PCT_TRAFFIC_DMH_UP", mac.PctTrafficDMHUp));
                parameters.Add(new SqlParameter("PCT_PLANT_SDMH_UP", mac.PctPlantSDMHUp));
                parameters.Add(new SqlParameter("PCT_PLANT_DMH_UP", mac.PctPlantDMHUp));
                parameters.Add(new SqlParameter("PCT_TRAFFIC_SDMH_DN", mac.PctTrafficSDMHDn));
                parameters.Add(new SqlParameter("PCT_TRAFFIC_DMH_DN", mac.PctTrafficDMHDn));
                parameters.Add(new SqlParameter("PCT_PLANT_SDMH_DN", mac.PctPlantSDMHDn));
                parameters.Add(new SqlParameter("PCT_PLANT_DMH_DN", mac.PctPlantDMHDn));
                parameters.Add(new SqlParameter("PCT_CPU_SDMH", mac.PctCpuSDMH));
                parameters.Add(new SqlParameter("PCT_CPU_DMH", mac.PctCpuDMH));
                parameters.Add(new SqlParameter("SUBSCRIBER_FIRST_NAME", mac.SubscriberFirstName));
                parameters.Add(new SqlParameter("SUBSCRIBER_LAST_NAME", mac.SubscriberLastName));
                parameters.Add(new SqlParameter("SUBSCRIBER_STREET_NUMBER", mac.SubscriberStreetNumber));
                parameters.Add(new SqlParameter("SUBSCRIBER_STREET_NAME", mac.SubscriberStreetName));
                parameters.Add(new SqlParameter("SUBSCRIBER_CITY", mac.SubscriberCity));
                parameters.Add(new SqlParameter("SUBSCRIBER_PHONE_NUMBER", mac.SubscriberPhoneNumber));

                id = Convert.ToInt32(SqlUtils.ExecuteScalar(query, parameters.ToArray(), databaseName));
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            return id;
        }

        /// <summary>
        /// Método para insertar la información de la cantidad de 
        /// bytes consumidos en el día por una MAC
        /// </summary>
        /// <param name="mac"></param>
        /// <returns></returns>
        public int InsertMacUsageByDay(MacUsageDTO mac)
        {
            int id = 0;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                string query = @"INSERT INTO  MAC_DAY_USAGE (
                                         DAY
                                        ,MAC
                                        ,KB
                                        ,KB_UP
                                        ,KB_DOWN
                                        )
                                     VALUES (
                                         @DAY
                                        ,@MAC
                                        ,@KB
                                        ,@KB_UP
                                        ,@KB_DOWN
                                      )";

                parameters.Add(new SqlParameter("DAY", mac.Day));
                parameters.Add(new SqlParameter("MAC", mac.Mac));
                parameters.Add(new SqlParameter("KB", mac.KiloBytes));
                parameters.Add(new SqlParameter("KB_UP", mac.KiloBytesUpstream));
                parameters.Add(new SqlParameter("KB_DOWN", mac.KiloBytesDownstream));

                id = Convert.ToInt32(SqlUtils.ExecuteScalar(query, parameters.ToArray(), databaseName));
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            return id;
        }

        /// <summary>
        /// Método para insertar la información de la cantidad de 
        /// bytes consumidos en el día por una MAC
        /// </summary>
        /// <param name="mac"></param>
        /// <returns></returns>
        public int InsertMacUsageByHour(MacUsageDTO mac)
        {
            int id = 0;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                string query = @"INSERT INTO  MAC_HOUR_USAGE (
                                         HOUR
                                        ,MAC
                                        ,KB
                                        ,KB_UP
                                        ,KB_DOWN
                                        )
                                     VALUES (
                                         @DAY
                                        ,@MAC
                                        ,@KB
                                        ,@KB_UP
                                        ,@KB_DOWN
                                      )";

                parameters.Add(new SqlParameter("DAY", mac.Day));
                parameters.Add(new SqlParameter("MAC", mac.Mac));
                parameters.Add(new SqlParameter("KB", mac.KiloBytes));
                parameters.Add(new SqlParameter("KB_UP", mac.KiloBytesUpstream));
                parameters.Add(new SqlParameter("KB_DOWN", mac.KiloBytesDownstream));

                id = Convert.ToInt32(SqlUtils.ExecuteScalar(query, parameters.ToArray(), databaseName));
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            return id;
        }
        #endregion
    }
}