﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;

using Oracle.DataAccess.Client;

using NOC.Desarrollo.Libraries.DTL.Dataminer;
using NOC.Desarrollo.Libraries.DTL.ServAssure;

namespace NOC.Desarrollo.Libraries.DAL.ServAssure
{
    /// <summary>
    /// Clase que contiene los métodos para obtener 
    /// información de ServAssure Advanced
    /// </summary>
    public class ServAssureDAO
    {
        private string connectionString = string.Empty;

        /// <summary>
        /// Método que obtiene los datos de una Mac
        /// </summary>
        /// <param name="mac">Mac del dispositivo</param>
        /// <returns></returns>
        public MacInformationDTO GetCmHourStatsByMac(bool isCv, string mac)
        {
            MacInformationDTO macInformation = new MacInformationDTO();
            macInformation.Mac = mac;
            macInformation.Hub = string.Empty;
            macInformation.Cmts = string.Empty;
            macInformation.Upstream = string.Empty;
            macInformation.Downstream = string.Empty;

            connectionString = isCv ?
                ConfigurationManager.ConnectionStrings["OracleCVConnectionString"].ConnectionString
                : ConfigurationManager.ConnectionStrings["OracleCMConnectionString"].ConnectionString;

            OracleConnection connection = new OracleConnection(connectionString);
            connection.Open();

            try
            {
                string query = @"SELECT MAC,HOUR,HUB,CMTS_NAME,UPSTREAM_NAME,DOWNSTREAM_NAME FROM STARGUS.CM_HOUR_STATS WHERE 1 = 1 AND rownum <= 1";
                query += string.Format(" AND MAC = '{0}'", mac.ToUpper());
                query += " ORDER BY HOUR DESC";

                using (OracleCommand command = new OracleCommand(query, connection))
                {
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            macInformation.Hub = reader.GetString(2);
                            macInformation.Cmts = reader.GetString(3);
                            macInformation.Upstream = reader.GetString(4);
                            macInformation.Downstream = reader.GetString(5);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                connection.Close();
            }
            return macInformation;
        }

        /// <summary>
        /// Método que obtiene la información de la vista
        /// MAC_DAY_USAGE
        /// </summary>
        /// <returns>Listado con el consumo de la MAC</returns>
        public List<MacUsageDTO> GetMacUsageByDay(bool isCv = false)
        {
            List<MacUsageDTO> macs = new List<MacUsageDTO>();

            connectionString = isCv ?
                ConfigurationManager.ConnectionStrings["OracleCVConnectionString"].ConnectionString
                : ConfigurationManager.ConnectionStrings["OracleCMConnectionString"].ConnectionString;

            OracleConnection connection = new OracleConnection(connectionString);
            connection.Open();
            try
            {
                string query = "SELECT DAY,MAC,KB,KB_UP,KB_DOWN,UPDATED FROM MAC_DAY_USAGE";

                using (OracleCommand command = new OracleCommand(query, connection))
                {
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            MacUsageDTO mac = new MacUsageDTO();
                            mac.Day = reader.GetDateTime(0);
                            mac.Mac = reader.GetString(1);
                            mac.KiloBytes = reader.IsDBNull(2) ? 0 : reader.GetDecimal(2);
                            mac.KiloBytesUpstream = reader.IsDBNull(3) ? 0 : reader.GetDecimal(3);
                            mac.KiloBytesDownstream = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);

                            if (!string.IsNullOrEmpty(mac.Mac))
                            {
                                macs.Add(mac);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                connection.Close();
            }

            return macs;
        }

        /// <summary>
        /// Método que obtiene la información de la vista
        /// MAC_DAY_USAGE
        /// </summary>
        /// <returns>Listado con el consumo de la MAC</returns>
        public List<MacUsageDTO> GetMacUsageByHour(bool isCv = false)
        {
            List<MacUsageDTO> macs = new List<MacUsageDTO>();

            connectionString = isCv ?
                ConfigurationManager.ConnectionStrings["OracleCVConnectionString"].ConnectionString
                : ConfigurationManager.ConnectionStrings["OracleCMConnectionString"].ConnectionString;

            OracleConnection connection = new OracleConnection(connectionString);
            connection.Open();
            try
            {
                string query = "SELECT HOUR,MAC,KB,KB_UP,KB_DOWN,UPDATED FROM MAC_HOUR_USAGE";

                using (OracleCommand command = new OracleCommand(query, connection))
                {
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            MacUsageDTO mac = new MacUsageDTO();
                            mac.Day = reader.GetDateTime(0);
                            mac.Mac = reader.GetString(1);
                            mac.KiloBytes = reader.IsDBNull(2) ? 0 : reader.GetDecimal(2);
                            mac.KiloBytesUpstream = reader.IsDBNull(3) ? 0 : reader.GetDecimal(3);
                            mac.KiloBytesDownstream = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);

                            if (!string.IsNullOrEmpty(mac.Mac))
                            {
                                macs.Add(mac);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                connection.Close();
            }

            return macs;
        }

        /// <summary>
        /// Método que obtiene la información de la vista
        /// CM_HOUR_HEALTH
        /// </summary>
        /// <param name="isCv"></param>
        /// <returns></returns>
        public List<MacHealthDTO> GetMacHealthByHour(bool isCv = false)
        {
            List<MacHealthDTO> macs = new List<MacHealthDTO>();

            connectionString = isCv ?
                ConfigurationManager.ConnectionStrings["OracleCVConnectionString"].ConnectionString
                : ConfigurationManager.ConnectionStrings["OracleCMConnectionString"].ConnectionString;

            OracleConnection connection = new OracleConnection(connectionString);
            connection.Open();
            try
            {
                string query = "SELECT MAC,ACCOUNT_NUMBER,HOUR_STAMP,TXPOWER_UP,RXPOWER_UP,RXPOWER_DN,";
                query += "CER_DN,CCER_DN,CER_UP,CCER_UP,SNR_DN,SNR_UP,CM_STATUS,PCT_TRAFFIC_SDMH_UP,PCT_TRAFFIC_DMH_UP,PCT_PLANT_SDMH_UP,PCT_PLANT_DMH_UP,";
                query += "PCT_TRAFFIC_SDMH_DN,PCT_TRAFFIC_DMH_DN,PCT_PLANT_SDMH_DN,PCT_PLANT_DMH_DN,PCT_CPU_SDMH,PCT_CPU_DMH,SUBSCRIBER_FIRST_NAME,SUBSCRIBER_LAST_NAME,";
                query += "SUBSCRIBER_STREET_NUMBER,SUBSCRIBER_STREET_NAME,SUBSCRIBER_CITY,SUBSCRIBER_PHONE_NUMBER FROM CM_HOUR_HEALTH WHERE 1 = 1 AND (CER_DN > 2 OR CER_UP > 2)";

                using (OracleCommand command = new OracleCommand(query, connection))
                {
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            MacHealthDTO mac = new MacHealthDTO();
                            mac.Mac = reader.GetString(0);
                            mac.AccountNumber = reader.GetString(1);
                            mac.HourStamp = reader.GetDateTime(2);
                            mac.TxPowerUp = reader.IsDBNull(3) ? 0 : reader.GetDouble(3);
                            mac.RxPowerUp = reader.IsDBNull(4) ? 0 : reader.GetDouble(4);
                            mac.RxPowerDn = reader.IsDBNull(5) ? 0 : reader.GetDouble(5);
                            mac.CerDn = reader.IsDBNull(6) ? 0 : reader.GetDouble(6);
                            mac.CCerDn = reader.IsDBNull(7) ? 0 : reader.GetDouble(7);
                            mac.CerUp = reader.IsDBNull(8) ? 0 : reader.GetDouble(8);
                            mac.CCerUp = reader.IsDBNull(9) ? 0 : reader.GetDouble(9);
                            mac.SnrDn = reader.IsDBNull(10) ? 0 : reader.GetDouble(10);
                            mac.SnrUp = reader.IsDBNull(11) ? 0 : reader.GetDouble(11);
                            mac.CmStatus = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                            mac.PctTrafficSDMHUp = reader.IsDBNull(13) ? 0 : reader.GetDouble(13);
                            mac.PctTrafficDMHUp = reader.IsDBNull(14) ? 0 : reader.GetDouble(14);
                            mac.PctPlantSDMHUp = reader.IsDBNull(15) ? 0 : reader.GetDouble(15);
                            mac.PctPlantDMHUp = reader.IsDBNull(16) ? 0 : reader.GetDouble(16);
                            mac.PctTrafficSDMHDn = reader.IsDBNull(17) ? 0 : reader.GetDouble(17);
                            mac.PctTrafficDMHDn = reader.IsDBNull(18) ? 0 : reader.GetDouble(18);
                            mac.PctPlantSDMHDn = reader.IsDBNull(19) ? 0 : reader.GetDouble(19);
                            mac.PctPlantDMHDn = reader.IsDBNull(20) ? 0 : reader.GetDouble(20);
                            mac.PctCpuSDMH = reader.IsDBNull(21) ? 0 : reader.GetDouble(21);
                            mac.PctCpuDMH = reader.IsDBNull(22) ? 0 : reader.GetDouble(22);
                            mac.SubscriberFirstName = reader.IsDBNull(23) ? string.Empty : reader.GetString(23);
                            mac.SubscriberLastName = reader.IsDBNull(24) ? string.Empty : reader.GetString(24);
                            mac.SubscriberStreetNumber = reader.IsDBNull(25) ? string.Empty : reader.GetString(25);
                            mac.SubscriberStreetName = reader.IsDBNull(26) ? string.Empty : reader.GetString(26);
                            mac.SubscriberCity = reader.IsDBNull(27) ? string.Empty : reader.GetString(27);
                            mac.SubscriberPhoneNumber = reader.IsDBNull(28) ? string.Empty : reader.GetString(28);

                            if (!string.IsNullOrEmpty(mac.Mac))
                            {
                                macs.Add(mac);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                connection.Close();
            }

            return macs;
        }

        /// <summary>
        /// Método que obtiene la información de la vista
        /// CM_HOUR_HEALTH
        /// </summary>
        /// <param name="isCv"></param>
        /// <returns></returns>
        public List<MacHealthDTO> GetMacHealthByMac(bool isCv = false, string macString = "")
        {
            List<MacHealthDTO> macs = new List<MacHealthDTO>();

            connectionString = isCv ?
                ConfigurationManager.ConnectionStrings["OracleCVConnectionString"].ConnectionString
                : ConfigurationManager.ConnectionStrings["OracleCMConnectionString"].ConnectionString;

            OracleConnection connection = new OracleConnection(connectionString);
            connection.Open();
            try
            {
                string query = "SELECT MAC,ACCOUNT_NUMBER,HOUR_STAMP,TXPOWER_UP,RXPOWER_UP,RXPOWER_DN,";
                query += "CER_DN,CCER_DN,CER_UP,CCER_UP,SNR_DN,SNR_UP,CM_STATUS,PCT_TRAFFIC_SDMH_UP,PCT_TRAFFIC_DMH_UP,PCT_PLANT_SDMH_UP,PCT_PLANT_DMH_UP,";
                query += "PCT_TRAFFIC_SDMH_DN,PCT_TRAFFIC_DMH_DN,PCT_PLANT_SDMH_DN,PCT_PLANT_DMH_DN,PCT_CPU_SDMH,PCT_CPU_DMH,SUBSCRIBER_FIRST_NAME,SUBSCRIBER_LAST_NAME,";
                query += "SUBSCRIBER_STREET_NUMBER,SUBSCRIBER_STREET_NAME,SUBSCRIBER_CITY,SUBSCRIBER_PHONE_NUMBER FROM CM_HOUR_HEALTH WHERE 1 = 1 AND MAC = '" + macString + "'";

                using (OracleCommand command = new OracleCommand(query, connection))
                {
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            MacHealthDTO mac = new MacHealthDTO();
                            mac.Mac = reader.GetString(0);
                            mac.AccountNumber = reader.GetString(1);
                            mac.HourStamp = reader.GetDateTime(2);
                            mac.TxPowerUp = reader.IsDBNull(3) ? 0 : reader.GetDouble(3);
                            mac.RxPowerUp = reader.IsDBNull(4) ? 0 : reader.GetDouble(4);
                            mac.RxPowerDn = reader.IsDBNull(5) ? 0 : reader.GetDouble(5);
                            mac.CerDn = reader.IsDBNull(6) ? 0 : reader.GetDouble(6);
                            mac.CCerDn = reader.IsDBNull(7) ? 0 : reader.GetDouble(7);
                            mac.CerUp = reader.IsDBNull(8) ? 0 : reader.GetDouble(8);
                            mac.CCerUp = reader.IsDBNull(9) ? 0 : reader.GetDouble(9);
                            mac.SnrDn = reader.IsDBNull(10) ? 0 : reader.GetDouble(10);
                            mac.SnrUp = reader.IsDBNull(11) ? 0 : reader.GetDouble(11);
                            mac.CmStatus = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                            mac.PctTrafficSDMHUp = reader.IsDBNull(13) ? 0 : reader.GetDouble(13);
                            mac.PctTrafficDMHUp = reader.IsDBNull(14) ? 0 : reader.GetDouble(14);
                            mac.PctPlantSDMHUp = reader.IsDBNull(15) ? 0 : reader.GetDouble(15);
                            mac.PctPlantDMHUp = reader.IsDBNull(16) ? 0 : reader.GetDouble(16);
                            mac.PctTrafficSDMHDn = reader.IsDBNull(17) ? 0 : reader.GetDouble(17);
                            mac.PctTrafficDMHDn = reader.IsDBNull(18) ? 0 : reader.GetDouble(18);
                            mac.PctPlantSDMHDn = reader.IsDBNull(19) ? 0 : reader.GetDouble(19);
                            mac.PctPlantDMHDn = reader.IsDBNull(20) ? 0 : reader.GetDouble(20);
                            mac.PctCpuSDMH = reader.IsDBNull(21) ? 0 : reader.GetDouble(21);
                            mac.PctCpuDMH = reader.IsDBNull(22) ? 0 : reader.GetDouble(22);
                            mac.SubscriberFirstName = reader.IsDBNull(23) ? string.Empty : reader.GetString(23);
                            mac.SubscriberLastName = reader.IsDBNull(24) ? string.Empty : reader.GetString(24);
                            mac.SubscriberStreetNumber = reader.IsDBNull(25) ? string.Empty : reader.GetString(25);
                            mac.SubscriberStreetName = reader.IsDBNull(26) ? string.Empty : reader.GetString(26);
                            mac.SubscriberCity = reader.IsDBNull(27) ? string.Empty : reader.GetString(27);
                            mac.SubscriberPhoneNumber = reader.IsDBNull(28) ? string.Empty : reader.GetString(28);

                            if (!string.IsNullOrEmpty(mac.Mac))
                            {
                                macs.Add(mac);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                connection.Close();
            }

            return macs;
        }
    }
}
