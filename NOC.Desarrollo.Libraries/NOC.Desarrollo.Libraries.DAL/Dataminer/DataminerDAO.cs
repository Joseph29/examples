﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using NOC.Desarrollo.Libraries.Core.Persistance;
using NOC.Desarrollo.Libraries.DTL.Dataminer;

namespace NOC.Desarrollo.Libraries.DAL.Dataminer
{
    /// <summary>
    /// Clase que contiene los métodos de
    /// Acceso a datos para la BD de 
    /// Dataminer
    /// </summary>
    public class DataminerDAO
    {
        private string databaseName = "Dataminer";

        #region Public
        /// <summary>
        /// Método para insertar una ubicación de una 
        /// MAC
        /// </summary>
        /// <param name="mac">Objeto de tio MacInformationDTO</param>
        /// <returns>Id de la MAC ingresada</returns>
        public int InsertMacInformation(MacInformationDTO mac)
        {
            int id = 0;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                string query = @"INSERT INTO  mac_information (
                                         MAC
                                        ,UPSTREAM_NAME
                                        ,DOWNSTREAM_NAME
                                        ,HUB_NAME
                                        ,CMTS_NAME
                                     )
                                     VALUES (
                                         @MAC
                                        ,@UPSTREAM_NAME
                                        ,@DOWNSTREAM_NAME
                                        ,@HUB_NAME
                                        ,@CMTS_NAME
                                      )
                                     SELECT @@Identity";

                parameters.Add(new SqlParameter("MAC", mac.Mac));
                parameters.Add(new SqlParameter("UPSTREAM_NAME", mac.Upstream));
                parameters.Add(new SqlParameter("DOWNSTREAM_NAME", mac.Downstream));
                parameters.Add(new SqlParameter("HUB_NAME", mac.Hub));
                parameters.Add(new SqlParameter("CMTS_NAME", mac.Cmts));

                id = Convert.ToInt32(SqlUtils.ExecuteScalar(query, parameters.ToArray(), databaseName));
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            return id;
        }

        /// <summary>
        /// Meptodo que regresa la lista de las ubicaciones de las
        /// MACs
        /// </summary>
        /// <returns>LIstado de MACs</returns>
        public List<MacInformationDTO> GetMacsInformation()
        {
            List<MacInformationDTO> macs = new List<MacInformationDTO>();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                string query = @"SELECT
                                         m.ID
                                        ,m.MAC
                                        ,m.UPSTREAM_NAME
                                        ,m.DOWNSTREAM_NAME
                                        ,m.HUB_NAME
                                        ,m.CMTS_NAME
                                    FROM mac_information as m
                                    WHERE 1 = 1";

                macs = SqlUtils.ExecuteList(query, parameters.ToArray(), databaseName, MacInformationMapper);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return macs;
        }

        /// <summary>
        /// Meptodo que actualiza la información de una
        /// MAC en específico
        /// </summary>
        /// <returns></returns>
        public bool UpdateMacInformation(MacInformationDTO mac)
        {
            bool success = true;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                string query = @"UPDATE mac_information
                                    SET
                                         UPSTREAM_NAME = @UPSTREAM_NAME
                                        ,DOWNSTREAM_NAME = @DOWNSTREAM_NAME
                                        ,HUB_NAME = @HUB_NAME
                                        ,CMTS_NAME = @CMTS_NAME
                                    WHERE 1 = 1
                                    AND ID = @ID";

                parameters.Add(new SqlParameter("ID", mac.MacInformationId));
                parameters.Add(new SqlParameter("UPSTREAM_NAME", mac.Upstream));
                parameters.Add(new SqlParameter("DOWNSTREAM_NAME", mac.Downstream));
                parameters.Add(new SqlParameter("HUB_NAME", mac.Hub));
                parameters.Add(new SqlParameter("CMTS_NAME", mac.Cmts));

                SqlUtils.ExecuteNonQuery(query, parameters.ToArray(), databaseName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return success;
        }

        /// <summary>
        /// Meptodo que elimina la información de una
        /// MAC en específico
        /// </summary>
        /// <returns></returns>
        public bool DeleteMacInformation()
        {
            bool success = true;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                string query = @"DELETE mac_information";

                SqlUtils.ExecuteNonQuery(query, parameters.ToArray(), databaseName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return success;
        }

        /// <summary>
        /// Meptodo que regresa la información de una
        /// MAC en específico
        /// </summary>
        /// <returns>Objeto de MACs</returns>
        public MacInformationDTO ExistsMacInformation(MacInformationRequestDTO request)
        {
            MacInformationDTO mac = new MacInformationDTO();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                string query = @"SELECT
                                         m.ID
                                        ,m.MAC
                                        ,m.UPSTREAM_NAME
                                        ,m.DOWNSTREAM_NAME
                                        ,m.HUB_NAME
                                        ,m.CMTS_NAME
                                    FROM mac_information as m
                                    WHERE 1 = 1";

                if (!string.IsNullOrEmpty(request.Mac))
                {
                    query += " AND m.MAC = @MAC";
                    parameters.Add(new SqlParameter("MAC", mac.Mac));
                }

                mac = SqlUtils.ExecuteObject(query, parameters.ToArray(), databaseName, MacInformationMapper);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return mac;
        }
        #endregion

        #region Mappers
        private MacInformationDTO MacInformationMapper(IDataReader reader)
        {
            return new MacInformationDTO
            {
                Mac = ReaderHelper.GetString(reader[0]),
                Upstream = ReaderHelper.GetString(reader[1]),
                Downstream = ReaderHelper.GetString(reader[2]),
                Hub = ReaderHelper.GetString(reader[3]),
                Cmts = ReaderHelper.GetString(reader[4])
            };
        }
        #endregion
    }
}
