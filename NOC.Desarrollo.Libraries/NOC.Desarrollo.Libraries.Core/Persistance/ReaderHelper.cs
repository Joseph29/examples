﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.Core.Persistance
{
    /// <summary>
    /// Clase que ocntien métodos para parsear diferentes
    /// tipos de datos
    /// </summary>
    public static class ReaderHelper
    {
        /// <summary>
        /// Método que devuelve un tipo de dato Entero
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetInteger(object value)
        {
            int result = 0;
            result = value == DBNull.Value ? 0 : Convert.ToInt32(value);
            return result;
        }

        /// <summary>
        /// Método que devuelve un tipo de dato Deciaml
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal GetDecimal(object value)
        {
            decimal result = 0;
            result = value == DBNull.Value ? 0 : Convert.ToDecimal(value);
            return result;
        }

        /// <summary>
        /// Método que devuelve un tipo de dato Long
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long GetLong(object value)
        {
            long result = 0;
            result = value == DBNull.Value ? 0 : Convert.ToInt64(value);
            return result;
        }

        /// <summary>
        /// Método que devuelve un tipo de dato String
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetString(object value)
        {
            string result = string.Empty;
            result = value == DBNull.Value ? "" : Convert.ToString(value);
            return result;
        }

        /// <summary>
        /// Método que devuelve un tipo de dato DateTime
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime GetDateTime(object value)
        {
            DateTime result = DateTime.MinValue;
            result = value == DBNull.Value ? result : Convert.ToDateTime(value);
            return result;
        }

        /// <summary>
        /// Método que devuelve un tipo de dato Boolean
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool GetBoolean(object value)
        {
            bool result = false;
            result = value == DBNull.Value ? result : Convert.ToBoolean(value);
            return result;
        }
    }
}
