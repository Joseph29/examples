﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.Core.Persistance
{
    public static partial class SqlUtils
    {
        #region ExecuteObject
        public static T ExecuteObject<T>(string sql, List<SqlParameter> @params, string db, Func<IDataReader, T> rowMapper)
        {
            return ExecuteObject(sql, @params.ToArray(), db, rowMapper);
        }

        public static T ExecuteObject<T>(string sql, SqlParameter[] @params, string db, Func<IDataReader, T> rowMapper)
        {
            var dr = ExecuteReader(sql, @params, db);
            return MapReaderToObject(dr, rowMapper);
        }

        /// <summary>
        /// Mapea el reader a un objeto.
        /// Ej: return SqlUtils.MapReaderToObject(reader, r => new Employee { Name = ProcessNull.GetString(r["name"]), Age = ProcessNull.GetInteger(r["age"]) });
        /// Solo la primera fila y luego cierra el reader.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="mapper">Una función que mapee el reader en la posición actual a un objeto T.</param>
        /// <returns>Un objeto T si había al menos una fila disponible en el reader o default(T) si no.</returns>
        public static T MapReaderToObject<T>(IDataReader reader, Func<IDataReader, T> mapper)
        {
            T obj = default(T);
            try
            {
                if (reader.Read())
                    obj = mapper(reader);
            }
            finally
            {
                reader.Dispose();
            }
            return obj;
        }
        #endregion

        #region ExecuteList
        public static List<T> ExecuteList<T>(string sql, List<SqlParameter> @params, string db, Func<IDataReader, T> rowMapper)
        {
            return ExecuteList(sql, @params.ToArray(), db, rowMapper);
        }

        public static List<T> ExecuteList<T>(string sql, SqlParameter[] @params, string db, Func<IDataReader, T> rowMapper)
        {
            return ExecuteList(sql, @params, db, rowMapper, null);
        }

        public static List<T> ExecuteList<T>(string sql, SqlParameter[] @params, string db, Func<IDataReader, T> rowMapper, int? timeOut)
        {
            IDataReader dr = ExecuteReader(sql, @params, db, timeOut);
            return MapReaderToList(dr, rowMapper);
        }

        /// <summary>
        /// Mapea el reader a una lista.
        /// Ej: return SqlUtils.MaReaderToList(reader, r => new Employee { Name = ProcessNull.GetString(r["name"]), Age = ProcessNull.GetInteger(r["age"]) });
        /// Mapea hasta que ya no se pueda leer del reader y luego lo cierra.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="mapper">Una función que mapee el reader en la posición actual a un objeto T.</param>
        /// <returns>Una lista con un objeto por cada fila disponible del reader.</returns>
        public static List<T> MapReaderToList<T>(IDataReader reader, Func<IDataReader, T> mapper)
        {
            var list = new List<T>();
            try
            {
                while (reader.Read())
                {
                    var item = mapper(reader);
                    list.Add(item);
                }
            }
            finally
            {
                reader.Dispose();
            }
            return list;
        }
        #endregion

        #region ExecuteReader
        public static IDataReader ExecuteReader(string sql, SqlParameter[] @params, string db)
        {
            return ExecuteReader(sql, @params, db, null);
        }

        public static IDataReader ExecuteReader(string sql, SqlParameter[] @params, string db, int? timeOut)
        {
            return SqlHelper.ExecuteReader(SelectConnection(db), CommandType.Text, sql, timeOut, @params);
        }
        #endregion

        #region ExecuteScalar
        public static object ExecuteScalar(string sql, string db)
        {
            var cn = new SqlConnection(SelectConnection(db));
            object result;

            try
            {
                var cmd = new SqlCommand(sql, cn);
                cn.Open();

                result = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cn.Dispose();
            }

            return result;
        }

        public static object ExecuteScalar(string sql, SqlParameter[] @params, string db)
        {
            var cn = new SqlConnection(SelectConnection(db));
            object result;

            try
            {
                var cmd = new SqlCommand(sql, cn);
                if (@params != null)
                    cmd.Parameters.AddRange(@params);
                cn.Open();

                result = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cn.Dispose();
            }

            return result;
        }
        #endregion

        #region ExecuteNonQuery
        public static int ExecuteNonQuery(string sql, List<SqlParameter> @params, string db, int? timeOut = null)
        {
            return ExecuteNonQuery(sql, @params.ToArray(), db, timeOut);
        }

        public static int ExecuteNonQuery(string sql, SqlParameter[] @params, string db, int? timeOut = null)
        {
            var cn = new SqlConnection(SelectConnection(db));
            int rowsAffected;

            try
            {
                var cmd = new SqlCommand(sql, cn);
                if (timeOut.HasValue) cmd.CommandTimeout = timeOut.Value;

                cmd.Parameters.AddRange(@params);
                cn.Open();

                rowsAffected = cmd.ExecuteNonQuery();
            }
            finally
            {
                cn.Dispose();
            }

            return rowsAffected;
        }

        public static int ExecuteNonQuery(string sql, SqlParameter[] @params, string db, SqlTransaction tr, int? timeOut = null)
        {
            if ((tr == null))
            {
                return ExecuteNonQuery(sql, @params, db, timeOut);
            }
            throw new NotSupportedException();
        }

        public static int ExecuteNonQuery(string sql, string db, int? timeOut = null)
        {
            var cn = new SqlConnection(SelectConnection(db));
            int rowsAffected;

            try
            {
                var cmd = new SqlCommand(sql, cn);
                if (timeOut.HasValue) cmd.CommandTimeout = timeOut.Value;
                cn.Open();


                rowsAffected = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cn.Dispose();
            }

            return rowsAffected;
        }
        #endregion

        #region PrivateMethods
        private static string SelectConnection(string databaseName)
        {
            return ConfigurationManager.ConnectionStrings[databaseName + "ConnectionString"].ConnectionString;
        }
        #endregion
    }
}
