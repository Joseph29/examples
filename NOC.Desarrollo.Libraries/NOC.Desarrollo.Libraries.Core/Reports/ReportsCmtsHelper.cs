﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using NOC.Desarrollo.Libraries.DTL.SevOne;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace NOC.Desarrollo.Libraries.Core.Reports
{
    /// <summary>
    /// Clase que contiene los métodos para generar reportes
    /// de los CMTS
    /// </summary>
    public static class ReportsCmtsHelper
    {
        #region Reporte Ancho de banda
        /// <summary>
        /// Método para generar el Reporte 
        /// de ancho de banda por interfaz
        /// </summary>
        /// <param name="device"></param>
        public static void ReportCMTSByDataUsed(DeviceDTO device)
        {
            string reportFile = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Reports\\" + device.DeviceName.Replace(' ', '-').Replace('/', '_') + ".xls";
            File.WriteAllBytes(reportFile, MemoryStreamReportCMTSByDataUsed(device).GetBuffer());
        }

        #region Private
        private static MemoryStream MemoryStreamReportCMTSByDataUsed(DeviceDTO device)
        {
            //Se genera el reporte
            var fs = new FileStream(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\NPOITemplate.xls", FileMode.Open, FileAccess.Read);
            var book = new HSSFWorkbook(fs, true);
            book.RemoveAt(0);

            //Create sheet for all the interfaces by CMTS
            var order = 0;

            #region Styles
            //Styles for data
            #region HeaderStyle
            var headerStyle = book.CreateCellStyle();
            headerStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Double;
            headerStyle.TopBorderColor = HSSFColor.White.Index;
            headerStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Double;
            headerStyle.LeftBorderColor = HSSFColor.White.Index;
            headerStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Double;
            headerStyle.RightBorderColor = HSSFColor.White.Index;
            headerStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Double;
            headerStyle.BottomBorderColor = HSSFColor.White.Index;
            headerStyle.FillBackgroundColor = HSSFColor.DarkBlue.Index;
            headerStyle.FillForegroundColor = HSSFColor.DarkBlue.Index;
            headerStyle.FillPattern = NPOI.SS.UserModel.FillPattern.BigSpots;
            headerStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            headerStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

            var headerFont = book.CreateFont();
            headerFont.FontName = HSSFFont.FONT_ARIAL;
            headerFont.FontHeightInPoints = (short)13;
            headerFont.Boldweight = (short)(0x2bc);
            headerFont.Color = HSSFColor.White.Index;
            headerStyle.SetFont(headerFont);
            #endregion

            #region FooterStyle
            var footerStyle = book.CreateCellStyle();
            footerStyle.FillBackgroundColor = HSSFColor.DarkBlue.Index;
            footerStyle.FillForegroundColor = HSSFColor.DarkBlue.Index;
            footerStyle.FillPattern = NPOI.SS.UserModel.FillPattern.BigSpots;
            footerStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

            var footerFont = book.CreateFont();
            footerFont.FontName = HSSFFont.FONT_ARIAL;
            footerFont.FontHeightInPoints = (short)12;
            footerFont.Boldweight = (short)(0x2bc);
            footerFont.Color = HSSFColor.White.Index;
            footerStyle.SetFont(footerFont);
            #endregion

            #region stringStyle
            var stringStyle = book.CreateCellStyle();
            stringStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Double;
            stringStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Double;
            stringStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Double;
            stringStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Double;

            var stringFont = book.CreateFont();
            stringFont.FontName = HSSFFont.FONT_ARIAL;
            stringFont.FontHeightInPoints = (short)8;
            stringStyle.SetFont(stringFont);
            #endregion

            #region centerStyle
            var centerStyle = book.CreateCellStyle();
            centerStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Double;
            centerStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Double;
            centerStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Double;
            centerStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Double;
            centerStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            centerStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            centerStyle.SetFont(stringFont);
            #endregion
            #endregion

            #region Reporte
            //Se comienza a realizar el reporte por interfaz
            foreach (var interfaz in device.DeviceObjects.OrderBy(x => x.DeviceObjectName))
            {
                try
                {
                    var maxRows = 0;
                    var rowsCount = 1;
                    var columnsCount = 1;

                    var sheet = book.CreateSheet(interfaz.DeviceObjectName.Replace(' ', '-').Replace('/', '-'));
                    book.SetSheetOrder(interfaz.DeviceObjectName.Replace(' ', '-').Replace('/', '-'), order);

                    var row = sheet.CreateRow(1);
                    #region Header
                    //Set header of each Interfaz
                    if (maxRows == 0)
                    {
                        row = sheet.CreateRow(rowsCount++);
                    }
                    else
                        row = sheet.GetRow(rowsCount++);

                    var cell = row.CreateCell(columnsCount, NPOI.SS.UserModel.CellType.String);
                    cell.SetCellValue(interfaz.DeviceObjectName);
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 1);
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 2);
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 3);
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 4);
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 5);
                    cell.CellStyle = headerStyle;
                    NPOI.SS.Util.CellRangeAddress merge = new NPOI.SS.Util.CellRangeAddress(rowsCount - 1, rowsCount - 1, columnsCount, columnsCount + 5);
                    sheet.AddMergedRegion(merge);

                    if (maxRows == 0)
                        row = sheet.CreateRow(rowsCount++);
                    else
                        row = sheet.GetRow(rowsCount++);

                    cell = row.CreateCell(columnsCount, NPOI.SS.UserModel.CellType.String);
                    cell.SetCellValue("CMTS");
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 1, NPOI.SS.UserModel.CellType.String);
                    cell.SetCellValue("INTERFAZ");
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 2, NPOI.SS.UserModel.CellType.String);
                    cell.SetCellValue("FECHA");
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 3, NPOI.SS.UserModel.CellType.String);
                    cell.SetCellValue("HORA");
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 4, NPOI.SS.UserModel.CellType.String);
                    cell.SetCellValue("IN OCTETS (Mb)");
                    cell.CellStyle = headerStyle;
                    cell = row.CreateCell(columnsCount + 5, NPOI.SS.UserModel.CellType.String);
                    cell.SetCellValue("OUT OCTETS (Mb)");
                    cell.CellStyle = headerStyle;
                    #endregion

                    foreach (var data in interfaz.DeviceObjectDataMax)
                    {
                        #region Data
                        if (maxRows == 0 || (maxRows > 0 && rowsCount >= maxRows))
                            row = sheet.CreateRow(rowsCount++);
                        else
                            row = sheet.GetRow(rowsCount++);

                        cell = row.CreateCell(columnsCount, NPOI.SS.UserModel.CellType.String);
                        cell.SetCellValue(device.DeviceName);
                        cell.CellStyle = centerStyle;
                        cell = row.CreateCell(columnsCount + 1, NPOI.SS.UserModel.CellType.String);
                        cell.SetCellValue(interfaz.DeviceObjectName);
                        cell.CellStyle = stringStyle;
                        cell = row.CreateCell(columnsCount + 2, NPOI.SS.UserModel.CellType.String);
                        cell.SetCellValue(data.DeviceDataDateTime.ToShortDateString());
                        cell.CellStyle = stringStyle;
                        cell = row.CreateCell(columnsCount + 3, NPOI.SS.UserModel.CellType.String);
                        cell.SetCellValue(data.DeviceDataDateTime.ToShortTimeString());
                        cell.CellStyle = stringStyle;
                        cell = row.CreateCell(columnsCount + 4, NPOI.SS.UserModel.CellType.Numeric);
                        cell.SetCellValue((double)data.DeviceDataHcInOctetsUsed / 1000 / 1000);
                        cell.CellStyle = stringStyle;
                        cell = row.CreateCell(columnsCount + 5, NPOI.SS.UserModel.CellType.Numeric);
                        cell.SetCellValue((double)data.DeviceDataHcOutOctetsUsed / 1000 / 1000);
                        cell.CellStyle = stringStyle;
                        #endregion

                        if (rowsCount > maxRows)
                            maxRows = rowsCount;

                        for (int i = 0; i < columnsCount; i++)
                        {
                            sheet.AutoSizeColumn(i);
                        }
                    }

                    columnsCount = columnsCount + 7;
                    order++;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("ReportByCMTS - {0}", ex.Message));
                }
            }

            #endregion

            var ms = new MemoryStream();
            book.Write(ms);
            return ms;
        }
        #endregion
        #endregion
    }
}
