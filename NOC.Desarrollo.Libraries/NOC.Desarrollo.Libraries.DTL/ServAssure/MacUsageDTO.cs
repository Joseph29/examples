﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.ServAssure
{
    /// <summary>
    /// Clase que contiene las propiedades del
    /// Ancho de banda consumido por una MAC
    /// </summary>
    public class MacUsageDTO
    {
        public DateTime Day { get; set; }
        public string Mac { get; set; }
        public decimal KiloBytes { get; set; }
        public decimal KiloBytesUpstream { get; set; }
        public decimal KiloBytesDownstream { get; set; }
    }
}
