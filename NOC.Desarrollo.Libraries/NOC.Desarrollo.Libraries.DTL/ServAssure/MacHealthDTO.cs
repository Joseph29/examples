﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.ServAssure
{
    /// <summary>
    /// Clase que contiene las propiedades del 
    /// estado de una MAC en un determinado periodo 
    /// de tiempo
    /// </summary>
    public class MacHealthDTO
    {
        public string Mac { get; set; }
        public string AccountNumber { get; set; }
        public DateTime HourStamp { get; set; }
        public double TxPowerUp { get; set; }
        public double RxPowerUp { get; set; }
        public double RxPowerDn { get; set; }
        public double CerDn { get; set; }
        public double CCerDn { get; set; }
        public double CerUp { get; set; }
        public double CCerUp { get; set; }
        public double SnrDn { get; set; }
        public double SnrUp { get; set; }
        public int CmStatus { get; set; }
        public double PctTrafficSDMHUp { get; set; }
        public double PctTrafficDMHUp { get; set; }
        public double PctPlantSDMHUp { get; set; }
        public double PctPlantDMHUp { get; set; }
        public double PctTrafficSDMHDn { get; set; }
        public double PctTrafficDMHDn { get; set; }
        public double PctPlantSDMHDn { get; set; }
        public double PctPlantDMHDn { get; set; }
        public double PctCpuSDMH { get; set; }
        public double PctCpuDMH { get; set; }
        public string SubscriberFirstName { get; set; }
        public string SubscriberLastName { get; set; }
        public string SubscriberStreetNumber { get; set; }
        public string SubscriberStreetName { get; set; }
        public string SubscriberCity { get; set; }
        public string SubscriberPhoneNumber { get; set; }
    }
}
