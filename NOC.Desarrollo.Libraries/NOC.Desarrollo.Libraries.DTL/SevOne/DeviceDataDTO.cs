﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.SevOne
{
    /// <summary>
    /// Clase que contiene los datos de cada
    /// lectura de una interfaz
    /// </summary>
    public class DeviceDataDTO
    {
        /// <summary>
        /// Fecha del evento
        /// </summary>
        public DateTime DeviceDataDateTime { get; set; }
        /// <summary>
        /// HC In Octets
        /// </summary>
        public decimal DeviceDataHcInOctets { get; set; }
        /// <summary>
        /// HC Out Octets
        /// </summary>
        public decimal DeviceDataHcOutOctets { get; set; }
        /// <summary>
        /// HC IN Octets Used
        /// </summary>
        public decimal DeviceDataHcInOctetsUsed { get; set; }
        /// <summary>
        /// HC Out Octets Used
        /// </summary>
        public decimal DeviceDataHcOutOctetsUsed { get; set; }
    }
}
