﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.SevOne
{
    /// <summary>
    /// Clase que contiene las elementos de cada
    /// dispositivo (interfaces, etc.)
    /// </summary>
    public class DeviceObjectDTO
    {
        /// <summary>
        /// Identificador del Dispositvo
        /// </summary>
        public int DeviceId { get; set; }
        /// <summary>
        /// Identificador del DeviceObject
        /// </summary>
        public int DeviceObjectId { get; set; }
        /// <summary>
        /// Nombre de las interfaz del dispositivo
        /// </summary>
        public string DeviceObjectName { get; set; }
        /// <summary>
        /// Descripción de las interfaz del dispositivo
        /// </summary>
        public string DeviceObjectDescription { get; set; }
        /// <summary>
        /// Indica si la interfaz esta habilitada
        /// </summary>
        public bool DeviceObjectIsEnabled { get; set; }
        /// <summary>
        /// Nombre de la tabla donde se guarda la informaciòn
        /// de la interfaz
        /// </summary>
        public string DeviceObjectTableName { get; set; }
        /// <summary>
        /// Nombre de la columna de IfHcInOctets
        /// </summary>
        public string DeviceColumnIfHcInOctets { get; set; }
        /// <summary>
        /// Nombre de la columna de IfHcOutOctets
        /// </summary>
        public string DeviceColumnIfHcOutOctets { get; set; }
        /// <summary>
        /// Lecturas de cada interfaz
        /// </summary>
        public List<DeviceDataDTO> DeviceObjectData { get; set; }
        /// <summary>
        /// Lecturas maximas de cada interfaz
        /// </summary>
        public List<DeviceDataDTO> DeviceObjectDataMax { get; set; }
    }
}
