﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.SevOne
{
    /// <summary>
    /// Clase que contiene las porpiedades de los
    /// dispositivos (CMTS, Sensores, etc)
    /// </summary>
    public class DeviceDTO
    {
        /// <summary>
        /// Identificador del dispositivo
        /// </summary>
        public int DeviceId { get; set; }
        /// <summary>
        /// Nombre del dispositivo
        /// </summary>
        public string DeviceName { get; set; }
        /// <summary>
        /// Descripción del dispositivo
        /// </summary>
        public string DeviceDescription { get; set; }
        /// <summary>
        /// Dirección IP del dispositivo
        /// </summary>
        public string DeviceIpAddress { get; set; }
        /// <summary>
        /// Comunidad SNMP del dispositivo
        /// </summary>
        public string DeviceCommunitySnmp { get; set; }
        /// <summary>
        /// Listado de las interfacez del dispositivo
        /// </summary>
        public List<DeviceObjectDTO> DeviceObjects { get; set; }
    }
}
