﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NOC.Desarrollo.Libraries.Common;
using NOC.Desarrollo.Libraries.Common.SevOne;

namespace NOC.Desarrollo.Libraries.DTL.SevOne
{
    /// <summary>
    /// Clase con los posibles filtros de una 
    /// consulta de datos por interfaz
    /// </summary>
    public class DeviceDataRequestDTO
    {
        /// <summary>
        /// Servidor
        /// </summary>
        public ServersEnum Server { get; set; }
        /// <summary>
        /// NOmbre de la tabla a consultar
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// Nombre de la columna de IfHcInOctets
        /// </summary>
        public string ColumnIfHcInOctets { get; set; }
        /// <summary>
        /// Nombre de la columna de IfHcOutOctets
        /// </summary>
        public string ColumnIfHcOutOctets { get; set; }
        /// <summary>
        /// Rango de fechas para la consulta
        /// </summary>
        public RangeDate RangeDateTime { get; set; }
    }
}
