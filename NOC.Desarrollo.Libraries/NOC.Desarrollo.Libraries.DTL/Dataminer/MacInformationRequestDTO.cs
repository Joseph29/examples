﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.Dataminer
{
    /// <summary>
    /// Clase que contiene las propiedades
    /// para filtrar una consulta
    /// </summary>
    public class MacInformationRequestDTO
    {
        public string Mac { get; set; }
    }
}
