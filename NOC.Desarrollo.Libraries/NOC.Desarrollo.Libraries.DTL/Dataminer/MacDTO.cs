﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.Dataminer
{
    /// <summary>
    /// Clase que contiene las propiedades
    /// de las MAC para Dataminer
    /// </summary>
    public class MacDTO
    {
        public int MacId { get; set; }
        public string Mac { get; set; }
        public string MacAddress { get; set; }
        public string MacColonia { get; set; }
        public string MacCP { get; set; }
        public DateTime MacLastUpdate { get; set; }
    }
}
