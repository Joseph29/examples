﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.Dataminer
{
    /// <summary>
    /// Clase que contiene las propiedades
    /// de la 
    /// </summary>
    public class MacInformationDTO
    {
        public int MacInformationId { get; set; }
        public string Mac { get; set; }
        public string Upstream { get; set; }
        public string Downstream { get; set; }
        public string Hub { get; set; }
        public string Cmts { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
