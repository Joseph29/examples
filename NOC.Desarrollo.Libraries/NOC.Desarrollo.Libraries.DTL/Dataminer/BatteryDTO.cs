﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.DTL.Dataminer
{
    /// <summary>
    /// Clase que contiene las propiedades
    /// de las Baterias
    /// </summary>
    public class BatteryDTO
    {
        public string BatteryIp { get; set; }
        public string BatteryMac { get; set; }
        public string TotalBattery { get; set; }
        public string BatteryVoltage { get; set; }
        public string BatteryVoltage2 { get; set; }
        public string BatteryVoltage3 { get; set; }
        public string InputVoltage { get; set; }
        public string BatteryComments { get; set; }
        public bool BatteryStatus { get; set; }
    }
}
