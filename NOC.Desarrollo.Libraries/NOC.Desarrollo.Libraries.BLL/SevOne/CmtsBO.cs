﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using NOC.Desarrollo.Libraries.Common;
using NOC.Desarrollo.Libraries.Common.SevOne;
using NOC.Desarrollo.Libraries.Core.Reports;
using NOC.Desarrollo.Libraries.DAL.SevOne;
using NOC.Desarrollo.Libraries.DTL.SevOne;

namespace NOC.Desarrollo.Libraries.BLL.SevOne
{
    /// <summary>
    /// Clase que contiene las reglas de negocio para
    /// los datos de los CMTS
    /// </summary>
    public class CmtsBO
    {
        /// <summary>
        /// Método para generar el reporte de interfaces
        /// por CMTS
        /// </summary>
        /// <param name="range"></param>
        /// <param name="server"></param>
        public void GenerateReportCMTS(RangeDate range, ServersEnum server)
        {
            try
            {
                List<DeviceDTO> devices = new List<DeviceDTO>();

                CmtsDAO dao = new CmtsDAO();
                devices = dao.GetCmts(server);
                foreach (var device in devices)
                {
                    Console.WriteLine(device.DeviceName);
                    device.DeviceObjects = dao.GetCmtsInterfacesById(server, device.DeviceId, true);
                    foreach (var objects in device.DeviceObjects)
                    {
                        if (!string.IsNullOrEmpty(objects.DeviceObjectTableName) && (!string.IsNullOrEmpty(objects.DeviceColumnIfHcInOctets) || !string.IsNullOrEmpty(objects.DeviceColumnIfHcOutOctets)))
                        {
                            DeviceDataDTO result = new DeviceDataDTO();
                            if (range != null)
                            {
                                for (var date = range.InitialDate.Value; date < range.FinalDate.Value; date = date.AddDays(1))
                                {
                                    result = new DeviceDataDTO();
                                    var data = dao.GetCmtsDataByInterfaz(
                                        new DeviceDataRequestDTO
                                        {
                                            TableName = objects.DeviceObjectTableName,
                                            RangeDateTime = new RangeDate { InitialDate = date.AddMinutes(-5), FinalDate = date.AddDays(1) },
                                            Server = server,
                                            ColumnIfHcInOctets = objects.DeviceColumnIfHcInOctets,
                                            ColumnIfHcOutOctets = objects.DeviceColumnIfHcOutOctets
                                        });
                                    #region Data
                                    result.DeviceDataDateTime = date;

                                    if (data.Count > 0)
                                    {
                                        for (int i = 1; i < data.Count; i++)
                                        {
                                            var seconds = (decimal)((data[i].DeviceDataDateTime - data[i - 1].DeviceDataDateTime).TotalSeconds);
                                            //Se valida los HcInOctet actual es cero
                                            if (data[i].DeviceDataHcInOctets == 0)
                                            {
                                                data[i].DeviceDataHcInOctetsUsed = 0;
                                            }
                                            else
                                            {
                                                if (seconds > 0)
                                                    data[i].DeviceDataHcInOctetsUsed = data[i - 1].DeviceDataHcInOctets == 0 ? 0 : (data[i].DeviceDataHcInOctets - data[i - 1].DeviceDataHcInOctets) * 8 / seconds;
                                            }

                                            //Se valida los HcOutOctet actual es cero
                                            if (data[i].DeviceDataHcOutOctets == 0)
                                            {
                                                data[i].DeviceDataHcOutOctetsUsed = 0;
                                            }
                                            else
                                            {
                                                if (seconds > 0)
                                                    data[i].DeviceDataHcOutOctetsUsed = data[i - 1].DeviceDataHcOutOctets == 0 ? 0 : (data[i].DeviceDataHcOutOctets - data[i - 1].DeviceDataHcOutOctets) * 8 / seconds;
                                            }
                                        }
                                        result.DeviceDataHcInOctetsUsed = data.Max(x => x.DeviceDataHcInOctetsUsed);
                                        result.DeviceDataHcOutOctetsUsed = data.Max(x => x.DeviceDataHcOutOctetsUsed);
                                    }
                                    objects.DeviceObjectDataMax.Add(result);
                                    #endregion
                                }
                            }
                            GenerateFileByInterfaceMax(device.DeviceName, objects);
                        }
                    }
                    ReportsCmtsHelper.ReportCMTSByDataUsed(device);
                    foreach (var aux in device.DeviceObjects)
                    {
                        aux.DeviceObjectDataMax = new List<DeviceDataDTO>();
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                Console.WriteLine(string.Format("GenerateReportCMTS - Server {0} - {1}", (int)server, ex.Message));
            }
        }

        #region GenerateReports
        public void GenerateFileByInterface(string name, DeviceObjectDTO deviceObject)
        {
            string file = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Results\\" + name.Replace(' ', '_').Replace('/', '_') + "-" + deviceObject.DeviceObjectName.Replace(' ', '_').Replace('/', '_').Replace('\\', '_').Replace('.', '_') + ".csv";

            try
            {
                if (!File.Exists(file))
                {
                    FileStream fs = File.Create(file);
                    fs.Close();
                }

                StreamWriter sw = File.AppendText(file);

                sw.WriteLine(string.Format("NAME,INTERFAZ,DATE,HOUR,IN_Octets,Out_Octets"));
                foreach (var measure in deviceObject.DeviceObjectData)
                {
                    sw.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", name
                        , deviceObject.DeviceObjectName
                        , measure.DeviceDataDateTime.ToShortDateString()
                        , measure.DeviceDataDateTime.ToShortTimeString()
                        , measure.DeviceDataHcInOctetsUsed / 1000 / 1000
                        , measure.DeviceDataHcOutOctetsUsed / 1000 / 1000));
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GenerateFileByInterface - Name {0} - {1}", name, ex.Message));
            }
        }

        public void GenerateFileByInterfaceMax(string name, DeviceObjectDTO deviceObject)
        {
            string file = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Results\\" + name.Replace(' ', '_') + "_" + deviceObject.DeviceObjectName.Replace(' ', '_').Replace('/', '_').Replace('\\', '_').Replace('.', '_') + ".csv";

            try
            {
                if (!File.Exists(file))
                {
                    FileStream fs = File.Create(file);
                    fs.Close();
                }

                StreamWriter sw = File.AppendText(file);

                sw.WriteLine(string.Format("NAME,INTERFAZ,DATE,IN_Octets,Out_Octets"));
                foreach (var measure in deviceObject.DeviceObjectDataMax)
                {
                    sw.WriteLine(string.Format("{0},{1},{2},{3},{4}", name, deviceObject.DeviceObjectName, measure.DeviceDataDateTime.ToShortDateString(), measure.DeviceDataHcInOctetsUsed / 1000 / 1000, measure.DeviceDataHcOutOctetsUsed / 1000 / 1000));
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GenerateFileByInterfaceMax - Name {0} - {1}", name, ex.Message));
            }
        }
        #endregion
    }
}
