﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using NOC.Desarrollo.Libraries.DAL.Dataminer;
using NOC.Desarrollo.Libraries.DAL.ServAssure;
using NOC.Desarrollo.Libraries.DTL.Dataminer;

namespace NOC.Desarrollo.Libraries.BLL.Dataminer
{
    /// <summary>
    /// Clase que contiene los métodos para importar
    /// y exportar información para Dataminer
    /// </summary>
    public class DataminerBO
    {
        #region Dataminer
        /// <summary>
        /// Método para actualizar los datos de cada una de las
        /// MACs
        /// </summary>
        /// <param name="fileName"></param>
        public void UpdateMacInformation(bool isCv, string fileName)
        {
            List<BatteryDTO> batteries = new List<BatteryDTO>();

            //Se obtienen los datos del archivo CSV
            batteries = ReadFile(fileName);

            if (batteries.Count > 0)
            {
                DataminerDAO dataminerDao = new DataminerDAO();
                ServAssureDAO servAssureDao = new ServAssureDAO();

                dataminerDao.DeleteMacInformation();
                foreach (var battery in batteries)
                {
                    MacInformationDTO mac = new MacInformationDTO();

                    #region Consultas ServAssure
                    mac = servAssureDao.GetCmHourStatsByMac(isCv, battery.BatteryMac);
                    mac.LastUpdate = DateTime.Now;
                    #endregion

                    #region TABLE: mac_information
                    dataminerDao.InsertMacInformation(mac);
                    #endregion
                }
            }
        }
        #endregion

        #region File
        /// <summary>
        /// Método donde se lee la información proporcionada por Dataminer
        /// para posteriormente procesarla
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns>Listado de Fuentes en el archivo</returns>
        public List<BatteryDTO> ReadFile(string fileName)
        {
            List<BatteryDTO> batteries = new List<BatteryDTO>();
            try
            {
                string file = string.Empty;
                file = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + string.Format("\\{0}", fileName);

                if (File.Exists(file))
                {
                    List<string> lines = File.ReadAllLines(file).ToList();
                    for (int i = 1; i < lines.Count; i++)
                    {
                        string line = lines[i];
                        if (!string.IsNullOrEmpty(line))
                        {
                            var split = line.Split(',');
                            BatteryDTO battery = new BatteryDTO();
                            battery.BatteryIp = split[3];
                            battery.BatteryMac = split[4];
                            battery.BatteryStatus = split[5] == "OK" ? true : false;
                            battery.TotalBattery = split[8];
                            battery.BatteryVoltage = split[9];
                            battery.BatteryVoltage2 = split[10];
                            battery.BatteryVoltage3 = split[11];
                            battery.InputVoltage = split[12];
                            battery.BatteryComments = split[split.Count() - 1];

                            batteries.Add(battery);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return batteries;
        }
        #endregion
    }
}