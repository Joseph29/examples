﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NOC.Desarrollo.Libraries.DAL.ServAssure;
using NOC.Desarrollo.Libraries.DTL.ServAssure;

namespace NOC.Desarrollo.Libraries.BLL.SevAssure
{
    /// <summary>
    /// Clase que contiene los métodos para 
    /// obtener información de ServAssure
    /// </summary>
    public class ServAssureBO
    {
        /// <summary>
        /// Método que insert la información del Estatus de las MACs
        /// por hora
        /// </summary>
        /// <param name="isCv">Indica si la conexión a Oracle corresponde
        /// a servidores del DF o de Interior de la República.</param>
        public void GetMacHealthByHour(bool isCv)
        {
            List<MacHealthDTO> macsHealth = new List<MacHealthDTO>();

            ServAssureDAO servAssureDao = new ServAssureDAO();
            macsHealth = servAssureDao.GetMacHealthByHour(isCv);

            MacHistoryDAO dao = new MacHistoryDAO();

            foreach (var mac in macsHealth)
                dao.InsertMacHealthByHour(mac);
        }

        /// <summary>
        /// Método que inserta la información del Estatus de las MACs
        /// por hora
        /// </summary>
        /// <param name="isCv">Indica si la conexión a Oracle corresponde
        /// a servidores del DF o de Interior de la República.</param>
        /// <param name="macs">Listado de macs a consultar.</param>
        public void GetMacHealthByHour(bool isCv, List<string> macs)
        {
            List<MacHealthDTO> macsHealth = new List<MacHealthDTO>();

            ServAssureDAO servAssureDao = new ServAssureDAO();
            foreach (var mac in macs)
            {
                macsHealth = servAssureDao.GetMacHealthByMac(isCv, mac);

                MacHistoryDAO dao = new MacHistoryDAO();
                foreach (var macHealth in macsHealth)
                    dao.InsertMacHealthByHour(macHealth);
            }
        }

        /// <summary>
        /// Método que inserta el tráfico en US y DS de las MACs
        /// por hora
        /// </summary>
        /// <param name="isCv">Indica si la conexión a Oracle corresponde
        /// a servidores del DF o de Interior de la República.</param>
        public void GetMacUsageByHour(bool isCv)
        {
            List<MacUsageDTO> macs = new List<MacUsageDTO>();

            ServAssureDAO servAssureDao = new ServAssureDAO();
            macs = servAssureDao.GetMacUsageByHour(isCv);

            MacHistoryDAO dao = new MacHistoryDAO();

            foreach (var mac in macs)
                dao.InsertMacUsageByHour(mac);

        }

        /// <summary>
        /// Método que inserta el tráfico en US y DS de las MACs
        /// por día
        /// </summary>
        /// <param name="isCv">Indica si la conexión a Oracle corresponde
        /// a servidores del DF o de Interior de la República.</param>
        public void GetMacUsageByDay(bool isCv)
        {
            List<MacUsageDTO> macs = new List<MacUsageDTO>();

            ServAssureDAO servAssureDao = new ServAssureDAO();
            macs = servAssureDao.GetMacUsageByDay(isCv);

            MacHistoryDAO dao = new MacHistoryDAO();

            foreach (var mac in macs)
                dao.InsertMacUsageByDay(mac);
        }
    }
}
