﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.Common
{
    /// <summary>
    /// Clase que declara el tipo de objecto
    /// RangeDate
    /// </summary>
    public class RangeDate
    {
        /// <summary>
        /// Fecha de inicio
        /// </summary>
        public DateTime? InitialDate { get; set; }
        /// <summary>
        /// Fecha fin
        /// </summary>
        public DateTime? FinalDate { get; set; }
    }
}
