﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NOC.Desarrollo.Libraries.Common.SevOne
{
    /// <summary>
    /// Enumerable con la cantidad de
    /// servidores que se encuentra dividido SevOne
    /// </summary>
    public enum ServersEnum
    {
        SevOne = 0,
        SevOne2,
        SevOne3,
        SevOne4,
        SevOne5,
        SevOne6,
        SevOneCm1,
        SevOneCm2,
        SevOneCm3
    }
}
